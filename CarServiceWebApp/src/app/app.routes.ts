import { Routes } from '@angular/router';
import { HomeComponent } from './features/home/home.component';
import { CustomersComponent } from './features/customers/customers-view/customers.component';
import { LoginComponent } from './features/login/login.component';
import { CarsComponent } from './features/cars/cars-view/cars.component';
import { authGuard } from './core/guard/auth.guard';

export const routes: Routes = [
    { path: 'home', component: HomeComponent, canActivate: [authGuard] },
    { path: 'customers', component: CustomersComponent, canActivate: [authGuard] },
    { path: 'cars', component: CarsComponent, canActivate: [authGuard] },
    { path: 'login', component: LoginComponent },
    { path: '', redirectTo: '/home', pathMatch: 'full'}
];

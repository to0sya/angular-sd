import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Car } from '../car';

@Injectable({
  providedIn: 'root'
})
export class CarsService {
  api_route: string = 'https://localhost:7200/cars';

  constructor(private httpClient: HttpClient) { }

  GetCars(): Observable<Car[]> {
    return this.httpClient.get<Car[]>(`${this.api_route}`);
  }

}

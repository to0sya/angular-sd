import { Component } from '@angular/core';
import { Car } from '../car';
import { CarsService } from '../cars-service/cars.service';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ExtendedCar } from '../extended-car';
import { AspirationFormatPipe } from '../../../core/aspiration-format/aspirationformat.pipe';
import { VolumeFormatPipe } from '../../../core/volume-format/volume-format.pipe';

@Component({
  selector: 'app-cars',
  standalone: true,
  imports: [CommonModule, FormsModule, AspirationFormatPipe, VolumeFormatPipe],
  templateUrl: './cars.component.html',
  styleUrl: './cars.component.css',
  providers: [CarsService]
})
export class CarsComponent {
  car: Car = {
    vin_code: '',
    brand_name: '',
    model_name: '',
    engine_volume: 0,
    fuel_type: '',
    aspiration: ''
  };

  cars: ExtendedCar[] = [];

  constructor(private CarsService: CarsService) { }

  ngOnInit(): void {
    this.CarsService.GetCars().subscribe(data => {
      console.log(data);
      this.cars = data.map(c => ({
        ...c,
        editMode: false
      }));
    });
  }
}

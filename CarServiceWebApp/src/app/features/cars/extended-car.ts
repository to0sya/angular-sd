import { Car } from "./car";

export interface ExtendedCar extends Car {
    editMode: boolean;
}

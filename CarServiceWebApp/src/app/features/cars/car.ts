export interface Car {
    vin_code: string;
    brand_name: string;
    model_name: string;
    engine_volume: number;
    fuel_type: string;
    aspiration: string;
}

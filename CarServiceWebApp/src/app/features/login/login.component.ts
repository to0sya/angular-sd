import { Component, SkipSelf, inject } from '@angular/core';
import { AuthService } from '../../core/services/auth.service';
import { CommonModule } from '@angular/common';
import { Login } from './login';
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'app-login',
  standalone: true,
  imports: [CommonModule, FormsModule],
  templateUrl: './login.component.html',
  styleUrl: './login.component.css',
  providers: []
})
export class LoginComponent {
  login: Login =  {
    username: '',
    password: ''
  }
  authService = inject(AuthService);

  onSubmit() {
    console.log("pososav");
    this.authService.login(this.login);
  }
}

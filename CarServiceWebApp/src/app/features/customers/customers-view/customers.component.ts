import { Component, OnInit } from '@angular/core';
import { CustomersService } from '../customers-service/customers-service.service';
import { Customer } from '../customer';
import { CommonModule } from '@angular/common';
import { CustomDateFormatPipe } from '../../../core/custom-date-format/customdateformat.pipe';
import { Car } from '../../cars/car';
import { FormsModule } from '@angular/forms';
import { ExtendedCustomer } from '../extended-customer';
import { RegExValidatorDirective } from '../../../shared/directives/reg-ex-validator.directive';
import { ValidationType } from '../../../shared/directives/validation-type';
import { CustomPhoneNumberPipe } from '../../../core/phone-number-format/custom-phone-number.pipe';

@Component({
  selector: 'app-customers',
  standalone: true,
  imports: [CommonModule, CustomDateFormatPipe, FormsModule, RegExValidatorDirective, CustomPhoneNumberPipe],
  templateUrl: './customers.component.html',
  styleUrl: './customers.component.css',
  providers: [CustomersService]
})
export class CustomersComponent implements OnInit {
  validationType = ValidationType;

  customer: Customer = {
    id: 0,
    name: '',
    surname: '',
    gender: '',
    birth_date: '',
    phone_number: '',
    cars: []
  };

  car: Car = {
    vin_code: '',
    brand_name: '',
    model_name: '',
    engine_volume: 0,
    fuel_type: '',
    aspiration: ''
  };
  
  customers: ExtendedCustomer[] = [];

  constructor(private CustomersService: CustomersService) { }
  ngOnInit(): void {
    this.CustomersService.GetCustomers().subscribe(data => {
      this.customers = data.map(c => ({
        ...c,
        editMode: false
      }));
    });
  }

  onSubmit() {
    console.log(this.customer);
    this.customer.cars.push(this.car);
    this.CustomersService.AddCustomer(this.customer).subscribe((data: { success: boolean; customer_id: number; }) => {
      if (data.success) {
        this.CustomersService.GetCustomer(data.customer_id).subscribe(customerData => {
          const extendedCustomer: ExtendedCustomer = {
            ...customerData,
            editMode: false
          };
          this.customers.push(extendedCustomer);
        });
      }
    });
    this.customer = { 
      id: 0,
      name: '',
      surname: '',
      gender: '',
      birth_date: '',
      phone_number: '',
      cars: []
    }
    this.car = {
      vin_code: '',
      brand_name: '',
      model_name: '',
      engine_volume: 0,
      fuel_type: '',
      aspiration: ''
    }
  }

  onDelete(id: number) {
    this.CustomersService.DeleteCustomer(id).subscribe((data: { success: boolean; }) => {
      if (data.success) {
        this.customers = this.customers.filter(customer => customer.id !== id);
      }
    });
  }
  
  onEdit(customer: Customer) {
    const index = this.customers.findIndex(c => c.id === customer.id);
    this.customers[index].editMode = true;
  }

  onSave(customer: ExtendedCustomer) {
    this.CustomersService.UpdateCustomer(customer).subscribe((data: { success: boolean; }) => {
      if (data.success) {
        const index = this.customers.findIndex(c => c.id === customer.id);
        this.customers[index] = customer;
        this.customers[index].editMode = false;
      }
    });
  }
  

  carsOutput(cars: Car[]): string {
    if (cars) {
      return cars.map(car => car.brand_name + ' ' + car.model_name).join(', ');
    } else {
      return '';
    }
  }

}

import { Customer } from "./customer";

export interface ExtendedCustomer extends Customer {
    editMode: boolean;
}

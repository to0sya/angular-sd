import { Car } from "../cars/car";

export interface Customer {
    id: number;
    name?: string;
    surname?: string;
    gender?: string;
    birth_date?: string;
    phone_number?: string;
    cars: Car[];
}

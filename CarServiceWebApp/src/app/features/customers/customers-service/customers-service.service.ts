import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Customer } from '../customer';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class CustomersService {
  api_route: string = 'https://localhost:7200/customers';

  httpHeaders = new HttpHeaders().set('Content-Type', 'application/json');

  constructor(private httpClient: HttpClient) { }

  AddCustomer(customer: Customer): any {
    return this.httpClient.post<Customer>(`${this.api_route}`, customer);
  }

  GetCustomers(): Observable<Customer[]> {
    return this.httpClient.get<Customer[]>(`${this.api_route}`);
  }

  GetCustomer(id: number): Observable<Customer> {
    return this.httpClient.get<Customer>(`${this.api_route}/${id}`);
  }

  UpdateCustomer(customer: Customer): any {
    return this.httpClient.put(`${this.api_route}/${customer.id}`, customer);
  }

  DeleteCustomer(id: number): any {
    return this.httpClient.delete(`${this.api_route}/${id}`);
  }


  
}

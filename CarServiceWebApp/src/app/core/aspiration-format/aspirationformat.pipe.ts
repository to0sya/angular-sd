import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'aspirationformat',
  standalone: true
})
export class AspirationFormatPipe implements PipeTransform {

  transform(value: string): string {
    if (value.includes('Natural')) {
      return 'Natural Aspiration';
    } else {
      return 'Turbocharged';
    }
  }
}

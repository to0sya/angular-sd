import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { Injectable } from '@angular/core';
import * as user_info from '../users.json';

@Injectable()
export class AuthInterceptor implements HttpInterceptor  {

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    console.log("HJelo");
    if (req.url === 'https://localhost:7200/authenticate' && req.method === 'POST') {
      if (user_info.username === req.body.username && user_info.password === req.body.password) {
        
        
        window.sessionStorage.setItem('authToken', user_info.token)
        
        console.log('Authenticated');

        return of(new HttpResponse({ status: 200, body: { success: true } }));
        
      } else {
        console.log('Invalid credentials');
        return of(new HttpResponse({ status: 200, body: { success: false } }));
      }
    }

    return next.handle(req);
  }
};

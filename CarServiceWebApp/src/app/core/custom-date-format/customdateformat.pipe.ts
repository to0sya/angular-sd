import { Pipe, PipeTransform } from '@angular/core';
import moment from 'moment';

@Pipe({
  name: 'customdateformat',
  standalone: true
})
export class CustomDateFormatPipe implements PipeTransform {

  transform(value: string): string {
    return moment(value).format('DD.MM.yyyy');
  }
}

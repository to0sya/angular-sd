import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Login } from '../../features/login/login';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor(private httpClient: HttpClient, private router: Router) {}

  login(user: Login) {
    const username = user.username;
    const password = user.password;

    this.httpClient.post('https://localhost:7200/authenticate', { username, password }).subscribe((response: any) => 
    {
      if (response.success) {
        this.router.navigate(['/home']);
      }
    });
  }

  logout() {
    sessionStorage.removeItem('authToken');
    this.router.navigate(['/login']);
  }

  isLoggedIn(): boolean {
    return sessionStorage.getItem('authToken') !== null;
  }
}

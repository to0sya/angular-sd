import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'volumeFormat',
  standalone: true
})
export class VolumeFormatPipe implements PipeTransform {

  transform(value: number): number {
    return value / 1000;
  }

}

import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'customPhoneNumber',
  standalone: true
})
export class CustomPhoneNumberPipe implements PipeTransform {

  transform(phone_number: string): string {
    phone_number = phone_number.replace('+', '');

    return phone_number.substring(0, 3) + 
    '(' + phone_number.substring(3, 5) + ')' + 
    phone_number.substring(5, 8) + '-' + 
    phone_number.substring(8, 10) + '-' + 
    phone_number.substring(10, 12);
  }
}

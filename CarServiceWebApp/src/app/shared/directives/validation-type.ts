export enum ValidationType {
    PhoneNumberValidation,
    PersonValidation,
    VinCodeValidation
}

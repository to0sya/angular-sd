import { Directive, Input, forwardRef } from '@angular/core';
import { AbstractControl, NG_VALIDATORS, ValidationErrors } from '@angular/forms';
import { ValidationType } from './validation-type';

@Directive({
  selector: '[appRegExValidator]',
  standalone: true,
  providers: [
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => RegExValidatorDirective),
      multi: true
    }
  ]
})
export class RegExValidatorDirective {
  @Input('appRegExValidator') regexString: string = '';
  @Input() validationType: ValidationType = ValidationType.PersonValidation;

  validate(control: AbstractControl): ValidationErrors | null {
    
    if (control.value) {
      const regex = new RegExp(this.regexString);
      const valid = regex.test(control.value);

      switch (this.validationType) {
        case ValidationType.PersonValidation: {
          return valid ? null : { ['invalidPattern']: 'Must start with a capital letter.' };
        }
        case ValidationType.PhoneNumberValidation: {
          return valid ? null : { ['invalidPattern']: 'Must be a valid phone number.' };
        }
        case ValidationType.VinCodeValidation: {
          return valid ? null : { ['invalidPattern']: 'Must be a valid VIN code.' };
        }
      }      
    }
    return null;
  }
}

import { Component, inject } from '@angular/core';
import { RouterLink, RouterLinkActive } from '@angular/router';
import { AuthService } from '../../core/services/auth.service';

@Component({
  selector: 'app-header-bar',
  standalone: true,
  imports: [RouterLink, RouterLinkActive],
  templateUrl: './header-bar.component.html',
  styleUrl: './header-bar.component.css'
})
export class HeaderBarComponent {
  authService = inject(AuthService);

  onLogout() {
    this.authService.logout();
  }
}
